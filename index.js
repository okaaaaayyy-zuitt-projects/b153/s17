let students = [];

function addStudent(student){
	students.push(student);
	console.log(`${student} has been added to the student list.`)
};

function countStudents(){
	console.log(`There are a total of ${students.length} students enrolled.`)
};

function printStudents(){
	students.sort()
	students.forEach(function(student){
		console.log(student)
	})
};

function findStudents(keyword){

	let matches = students.filter(function(student){
		return student.toLowerCase().includes(keyword.toLowerCase())
	})

	if(matches.length === 1){
		console.log(matches + " is an enrollee.")
	}else if(matches.length > 1){
		matches = matches.join(",")	
		console.log(matches + "are enrollees.")
	}else{
		console.log("No student found with the name " + keyword)
	}
}

function addSection(section){

	let studentSections = students.map(function(student){
		return student + ' - section ' + section
	})
	console.log(studentSections);
}

function removeStudent(student){
	student = student.toLowerCase()

	let lowerCaseNames = students.map(function(student){
		return student.toLowerCase()
	})

	let studentIndex = lowerCaseNames.indexOf(student)

	lowerCaseNames.splice(studentIndex, 1)

	console.log(student + " was removed from the students list.")
	console.log(lowerCaseNames)
}

